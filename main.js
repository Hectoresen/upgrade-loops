

//Iteración #1: Usa includes


/*
const products = ['Camiseta de Pokemon', 'Pantalón coquinero', 'Gorra de gansta', 'Camiseta de Basket', 'Cinrurón de Orión', 'AC/DC Camiseta'];

products.forEach(element => {
    if(element.includes('Camiseta')){
        console.log(element);
    }
});
*/


//Iteración #2: Condicionales avanzados
//True es un 1
//False es un 0

/*
const alumns = [
    {name: 'Pepe Viruela', T1: false, T2: false, T3: true,},
		{name: 'Lucia Aranda', T1: true, T2: false, T3: true},
		{name: 'Juan Miranda', T1: false, T2: true, T3: true},
		{name: 'Alfredo Blanco', T1: false, T2: false, T3: false},
		{name: 'Raquel Benito', T1: true, T2: true, T3: true}
]


alumns.forEach(alumn => {
    let contador = 0;
    //         IF            ELSE
    //if(alumn.T1 == true){
    //  contador++;
    //}else{
    //}

    (alumn.T1) ? contador++ : "" ;
    (alumn.T2) ? contador++ : "" ;
    (alumn.T3) ? contador++ : "" ;

    // Si contador >= 2 Crea "Final" con valor "aprobado". Si no, crea "Final" con valor "suspenso"
    (contador >= 2) ? alumn.Final = "aprobado" : alumn.Final = "suspenso";
});

console.log(alumns);

*/

//Iteración #3: Probando For...of

/*
const placesToTravel = ['Japon', 'Venecia', 'Murcia', 'Santander', 'Filipinas', 'Madagascar'];

for (const element of placesToTravel) {
    console.log(element);
}

*/

//Iteración #4: Probando For...in

/*
const alien = {
    name: 'Wormuck',
    race: 'Cucusumusu',
    planet: 'Eden',
    weight: '259kg'
}
for (const element in alien) {
    console.log(`${element}: ${alien[element]}`);
}
*/

//Iteración #5: Probando For

/*
const placesToTravel = [
    {id: 5, name: 'Japan'},
    {id: 11, name: 'Venecia'},
    {id: 23, name: 'Murcia'},
    {id: 40, name: 'Santander'},
    {id: 44, name: 'Filipinas'},
    {id: 59, name: 'Madagascar'}
];

for(let i = 0; i <placesToTravel.length; i++){

    (placesToTravel[i]['id'] == 11) || (placesToTravel[i]['id'] == 5) ? delete placesToTravel[i] : '';

    //if((placesToTravel[i]['id'] == 11) || (placesToTravel[i]['id'] == 5) ){
    //    delete placesToTravel[i];
    //}

}
console.log(placesToTravel);

*/

//Iteración #6: Mixed For...of e includes


/* const toys = [
    {id: 5, name: 'Buzz MyYear'},
    {id: 11, name: 'Action Woman'},
    {id: 23, name: 'Barbie Man'},
    {id: 40, name: 'El gato con Guantes'},
    {id: 40, name: 'El gato felix'}
]
for (let i of toys) {
//     if(element['name'].includes('gato')){
//        console.log(element);
//        toys.pop()
//      toys.pop()
//    }
    (i['name'].includes('gato')) ? (toys.pop()) && (toys.pop()) : '';
}
console.log(toys);

// O esta opción:
const toys = [
	{id: 5, name: 'Buzz MyYear', sellCount: 10},
	{id: 11, name: 'Action Woman', sellCount: 24},
	{id: 23, name: 'Barbie Man', sellCount: 15},
	{id: 40, name: 'El gato con Guantes', sellCount: 8},
	{id: 40, name: 'El gato felix', sellCount: 35}
]
var newToys = [];

for (const i of toys) {
    if(i['name'].includes('gato')){
        console.log('Esto es un gato');
    }else{
        newToys.push(i);
    }
}
console.log(newToys);

 */

//Iteración #7: For...of avanzado


/*
const popularToys = [];
const toys = [
	{id: 5, name: 'Buzz MyYear', sellCount: 10},
	{id: 11, name: 'Action Woman', sellCount: 24},
	{id: 23, name: 'Barbie Man', sellCount: 15},
	{id: 40, name: 'El gato con Guantes', sellCount: 8},
	{id: 40, name: 'El gato felix', sellCount: 35}
]

for (const i of toys) {
//    if(i['sellCount'] >= 15){
//        popularToys.push(i);
//    }
    (i['sellCount'] >= 15) ? popularToys.push(i) : '';
}
console.log(popularToys);
*/